package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class UserDao implements Dao<User>{
	Properties appProps = new Properties();

	public UserDao() {
		try {
			appProps.load(new FileInputStream("app.config"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} //this is a constructor for the UserDao.java class

	//Once all of the properties are set in 
	//the business logic for a user, 
	//the DAO sends the data to the database.
	//DAO = Data Access Object, provides an
	//abstract interface to a database.
	@Override
	public void create(User objectToSave) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))){
			
			CallableStatement stmt = cn.prepareCall("{call CreateUser(?,?,?,?)}");
			int n = 0;
			stmt.setString(++n, objectToSave.getLastName());
			stmt.setString(++n, objectToSave.getFirstName());
			stmt.setString(++n, objectToSave.getPassword());
			stmt.setString(++n,  objectToSave.getEmail());
			stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public List<User> getALL() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User get(int id) {
		return null;
	}

	@Override
	public void update(User objectToSave) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(User objectToSave) {
		// TODO Auto-generated method stub
		
	}

	public User get(String pEmail, String pPassword) {
		User loadUser = new User();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetUserByEmailAndPassword(?,?)}");
			int n = 0;
			stmt.setString(++n, pEmail); 
			stmt.setString(++n, pPassword);
			ResultSet rsUser = stmt.executeQuery();
			
			if (rsUser.next()) {
				loadUser.setUserID(rsUser.getInt("UserID"));
				loadUser.setLastName(rsUser.getString("LastName"));
				loadUser.setFirstName(rsUser.getString("FirstName"));
				loadUser.setPassword(rsUser.getString("Password"));
				loadUser.setEmail(rsUser.getString("Email"));
			}
		} catch (SQLException e) {
			e.printStackTrace();                                                                            
		}
		return loadUser;
	}

	public List<User> getAllbyID(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}
