package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class AddressDao implements Dao<Address> {
	Properties appProps = new Properties();
	private List<Address> allAddresses = new ArrayList<>();
	public List<Address> getAllBooks() {
		return allAddresses;
	}
	
	public AddressDao() {
		try {
			appProps.load(new FileInputStream("app.config"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} //this is a constructor for the AddressDao.java class
	
	@Override
	public void create(Address objectToSave) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))){
			
			CallableStatement stmt = cn.prepareCall("{call CreateUserAddress(?,?,?,?,?,?,?,?,?)}");
			int n = 0;
			stmt.setString(++n, objectToSave.getAddressLineOne());
			stmt.setString(++n, objectToSave.getAddressLineTwo());
			stmt.setString(++n, objectToSave.getCity());
			stmt.setString(++n, objectToSave.getState());
			stmt.setString(++n, objectToSave.getZip());
			stmt.setString(++n, objectToSave.getCountry());
			stmt.setString(++n, objectToSave.getAddressLastName());
			stmt.setString(++n, objectToSave.getAddressFirstName());
			stmt.setInt(++n, objectToSave.getUserID());
			stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public List<Address> getALL() {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {
			CallableStatement stmt = cn.prepareCall("{call GetAllAddresses()}");
			ResultSet rsAddress = stmt.executeQuery();
			while (rsAddress.next()) {
				Address myAddresses = new Address();
				myAddresses.setAddressID(rsAddress.getInt("AddressID"));
				myAddresses.setAddressLineOne(rsAddress.getString("AddressLine1"));
				myAddresses.setAddressLineTwo(rsAddress.getString("AddressLine2"));
				myAddresses.setCity(rsAddress.getString("City"));
				myAddresses.setState(rsAddress.getString("State"));
				myAddresses.setZip(rsAddress.getString("Zip"));
				myAddresses.setCountry(rsAddress.getString("Country"));
				myAddresses.setAddressLastName(rsAddress.getString("AddressLastName"));
				myAddresses.setAddressFirstName(rsAddress.getString("AddressFirstName"));
				allAddresses.add(myAddresses);
			}
		} catch (SQLException e) {
			e.printStackTrace();                                                                            
		}
		return allAddresses;
	}

	@Override
	public Address get(int pAddressId) {
		Address returnAddress = new Address();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {
			
			CallableStatement stmt = cn.prepareCall("{call GetAddressByAddressID(?)}");
			int n = 0;
			stmt.setInt(++n, pAddressId);
			ResultSet rsAddress = stmt.executeQuery();
			
			if (rsAddress.next()) {
				returnAddress.setAddressID(rsAddress.getInt("AddressID"));
				returnAddress.setAddressLineOne(rsAddress.getString("AddressLine1"));
				returnAddress.setAddressLineTwo(rsAddress.getString("AddressLine2"));
				returnAddress.setCity(rsAddress.getString("City"));
				returnAddress.setState(rsAddress.getString("State"));
				returnAddress.setZip(rsAddress.getString("Zip"));
				returnAddress.setCountry(rsAddress.getString("Country"));
				returnAddress.setAddressLastName(rsAddress.getString("AddressLastName"));
				returnAddress.setAddressFirstName(rsAddress.getString("AddressFirstName"));
			}
		} catch (SQLException e) {
			e.printStackTrace();                                                                            
		}
		return returnAddress;
	}

	@Override
	public void update(Address objectToUpdate) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call UpdateAddressByAddressID(?,?,?,?,?,?,?,?,?)}");
			int n = 0;
			stmt.setInt(++n, objectToUpdate.getAddressID());
			stmt.setString(++n, objectToUpdate.getAddressLineOne());
			stmt.setString(++n, objectToUpdate.getAddressLineTwo());
			stmt.setString(++n, objectToUpdate.getCity());
			stmt.setString(++n, objectToUpdate.getState());
			stmt.setString(++n, objectToUpdate.getZip());
			stmt.setString(++n, objectToUpdate.getCountry());
			stmt.setString(++n, objectToUpdate.getAddressLastName());
			stmt.setString(++n, objectToUpdate.getAddressFirstName());
			stmt.executeQuery();
			int rowsAffected = stmt.executeUpdate();
			if (rowsAffected != 1 ) {
				throw new InvalidParameterException("This update affected " + rowsAffected + " rows.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void delete(Address objectToDelete) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call DeleteAddressByAddressID(?)}");
			int n = 0;
			stmt.setInt(++n, objectToDelete.getAddressID());
			stmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public Address get(String email, String password) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Address> getAllbyID(int pUserID) {
		List<Address> allUserAddresses = new ArrayList<>();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {
			
			CallableStatement stmt = cn.prepareCall("{call GetAllAddressesByUserID(?)}");
			int n = 0;
			stmt.setInt(++n, pUserID);
			ResultSet rsAddress = stmt.executeQuery();
			
			while (rsAddress.next()) {
				Address myAddresses = new Address();
				myAddresses.setAddressID(rsAddress.getInt("AddressID"));
				myAddresses.setAddressLineOne(rsAddress.getString("AddressLine1"));
				myAddresses.setAddressLineTwo(rsAddress.getString("AddressLine2"));
				myAddresses.setCity(rsAddress.getString("City"));
				myAddresses.setState(rsAddress.getString("State"));
				myAddresses.setZip(rsAddress.getString("Zip"));
				myAddresses.setCountry(rsAddress.getString("Country"));
				myAddresses.setAddressLastName(rsAddress.getString("AddressLastName"));
				myAddresses.setAddressFirstName(rsAddress.getString("AddressFirstName"));
				allUserAddresses.add(myAddresses);
			}
		} catch (SQLException e) {
			e.printStackTrace();                                                                            
		}
		return allUserAddresses;
	}

}
