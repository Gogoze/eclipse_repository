package model;

public class Genre {
	private int genreID;
	private String genreName;
	
	public Genre() {
		
	}
	
	public Genre(int genreID, String genreName) {
		this.genreID = genreID;
		this.genreName = genreName;
	}
	
	
	/**
	 * @return the genreID
	 */
	public final int getGenreID() {
		return genreID;
	}
	/**
	 * @param genreID the genreID to set
	 */
	public final void setGenreID(int genreID) {
		this.genreID = genreID;
	}

	/**
	 * @return the genreName
	 */
	public final String getGenreName() {
		return genreName;
	}
	/**
	 * @param genreName the genreName to set
	 */
	public final void setGenreName(String genreName) {
		this.genreName = genreName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Genre [genreID=" + genreID + ", genreName=" + genreName + "]";
	}
	
	
}