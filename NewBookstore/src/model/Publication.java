package model;

import java.time.LocalDate;

public class Publication {
	//from the publisher table
		/**
		 * This is the addressID.
		 */
		private int addressID;
		/**
		 * This is the publication name.
		 */
		private String publisherName;
		/**
		 * This is the contactName.
		 */
		private String contactName;
		/**
		 * This is the phoneID.
		 */
		private int phoneId;
		/**
		 * This is the copyright date.
		 */
		private LocalDate copyrightDate;
		
		
		/**
		 * @return the addressID
		 */
		public final int getAddressID() {
			return addressID;
		}
		/**
		 * @param addressID the addressID to set
		 */
		public final void setAddressID(final int addressID) {
			this.addressID = addressID;
		}
		/**
		 * @return the publisherName
		 */
		public final String getPublisherName() {
			return publisherName;
		}
		/**
		 * @param publisherName the publisherName to set
		 */
		public final void setPublisherName(final String publisherName) {
			this.publisherName = publisherName;
		}
		/**
		 * @return the contactName
		 */
		public final String getContactName() {
			return contactName;
		}
		/**
		 * @param contactName the contactName to set
		 */
		public final void setContactName(final String contactName) {
			this.contactName = contactName;
		}
		/**
		 * @return the phoneId
		 */
		public final int getPhoneId() {
			return phoneId;
		}
		/**
		 * @param phoneId the phoneId to set
		 */
		public final void setPhoneId(final int phoneId) {
			this.phoneId = phoneId;
		}
		/**
		 * @return the copyrightDate
		 */
		public final LocalDate getCopyrightDate() {
			return copyrightDate;
		}
		/**
		 * @param copyrightDate the copyrightDate to set
		 */
		public final void setCopyrightDate(final LocalDate copyrightDate) {
			this.copyrightDate = copyrightDate;
		}
		
}
