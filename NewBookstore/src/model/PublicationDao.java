package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.Properties;

public class PublicationDao implements Dao<Publication>{
	Properties appProps = new Properties();
	
	public PublicationDao() {
		try {
			appProps.load(new FileInputStream("app.config"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public Publication get(int pPublicationID) {
		Publication returnPublication = new Publication();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetPublisherByPublicationID(?)}");
			int n = 0;
			stmt.setInt(++n, pPublicationID);
			ResultSet rsPublication = stmt.executeQuery();

			if (rsPublication.next()) {
				returnPublication.setPhoneId(rsPublication.getInt("PublicationID"));
				returnPublication.setAddressID(rsPublication.getInt("AddressID"));
				returnPublication.setPublisherName(rsPublication.getString("PublisherName"));
				returnPublication.setContactName(rsPublication.getString("ContactName"));
				returnPublication.setPhoneId(rsPublication.getInt("PhoneID"));
				returnPublication.setCopyrightDate(LocalDate.parse(rsPublication.getString("CopyrightDate")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnPublication;
	}
/*
	@Override
	public void create(Publication objectToSave) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"), 
				appProps.getProperty("userName"), appProps.getProperty("password"))){
			
			CallableStatement stmt = cn.prepareCall("{call CreateBook(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			int n = 0;
			stmt.setInt(++n, objectToSave.geta());
			stmt.setString(++n, objectToSave.getBookName());
			stmt.setString(++n, objectToSave.getAuthor());
			stmt.setDouble(++n, objectToSave.getPrice());
			stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Publication> getALL() {
		// TODO Auto-generated method stub
		return null;
	}

	

	@Override
	public void update(Publication objectToUpdate) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call UpdateBookByISBN(?,?,?,?)}");
			int n = 0;
			stmt.setInt(++n, objectToUpdate.getISBN());
			stmt.setString(++n, objectToUpdate.getBookName());
			stmt.setString(++n, objectToUpdate.getAuthor());
			stmt.setDouble(++n, objectToUpdate.getPrice());
			int rowsAffected = stmt.executeUpdate();
			if (rowsAffected != 1 ) {
				throw new InvalidParameterException("This update affected " + rowsAffected + " rows.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void delete(Publication objectToDelete) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call DeleteBookByISBN(?)}");
			int n = 0;
			stmt.setInt(++n, objectToDelete.getISBN());
			stmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	*/

	//Trash v v v v v
	@Override
	public void create(Publication objectToSave) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Publication> getALL() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void update(Publication objectToSave) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Publication objectToSave) {
		// TODO Auto-generated method stub
		
	}
}