package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class PaymentDao implements Dao<Payment>{
Properties appProps = new Properties();
	
	public PaymentDao() {
		try {
			appProps.load(new FileInputStream("app.config"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} //this is a constructor for the AddressDao.java class

	@Override
	public void create(Payment objectToSave) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))){
			
			CallableStatement stmt = cn.prepareCall("{call CreateUserPayment(?,?,?,?,?,?)}");
			int n = 0;
			stmt.setString(++n, objectToSave.getCardNumber());
			stmt.setDate(++n, objectToSave.getExpDate());
			stmt.setString(++n, objectToSave.getCvv());
			stmt.setString(++n, objectToSave.getCardType());
			stmt.setString(++n, objectToSave.getActive());
			stmt.setInt(++n, objectToSave.getUserID());
			stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Payment> getALL() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Payment get(int pPaymentID) {
		Payment returnPayment = new Payment();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {
			
			CallableStatement stmt = cn.prepareCall("{call GetPaymentByPaymentID(?)}");
			int n = 0;
			stmt.setInt(++n, pPaymentID);
			ResultSet rsPayment = stmt.executeQuery();
			
			if (rsPayment.next()) {
				returnPayment.setPaymentID(rsPayment.getInt("PaymentID"));
				returnPayment.setAddressID(rsPayment.getInt("AddressID"));
				returnPayment.setCardNumber(rsPayment.getString("CardNumber"));
				returnPayment.setExpDate(rsPayment.getDate("ExpDate"));
				returnPayment.setCvv(rsPayment.getString("CVV"));
				returnPayment.setCardType(rsPayment.getString("CardType"));
				returnPayment.setActive(rsPayment.getString("Active"));
				returnPayment.setUserID(rsPayment.getInt("UserID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();                                                                            
		}
		return returnPayment;
	}

	public Payment get(String email, String password) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Payment objectToSave) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Payment objectToDelete) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call DeletePaymentByPaymentID(?)}");
			int n = 0;
			stmt.setInt(++n, objectToDelete.getPaymentID());
			stmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public List<Payment> getAllbyID(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
