package model;
/**
 * This class is used to create a user in the database.
 * @author Caleb
 *
 */
public class User {
	/**
	 * This declaration will aggregate the class
	 */
	private Address[] addresses = new Address[0];
	
	/**
 	* lastName variable
 	*/
	private String lastName;
	/**
	 * firstName variable
	 */
	private String firstName;
	/**
	 * the password will be a string variable
	 */
	private String password;
	/**
	 * the email will be a string variable
	 */
	private String email;
	/**
	 * This variable will be a place holder for
	 * the ID for the user being grabbed from the
	 * database.
	 */
	private int userID;
	/**
	 * getter for the lastName
	 * @return lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * setter for the lastName
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * getter for the firstName
	 * @return firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * setter for the firstName
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * getter for the password
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * setter for the password
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * getter for the email
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * setter for the email
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * getter for the userID
	 * @return
	 */
	public int getUserID() {
		return userID;
	}
	/**
	 * setter for the userID
	 * @param userID
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}
	
}
