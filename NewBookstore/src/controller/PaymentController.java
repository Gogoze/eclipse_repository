package controller;

import java.sql.Date;
import java.time.LocalDate;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import model.Address;
import model.AddressDao;
import model.Payment;
import model.PaymentDao;
import model.User;

public class PaymentController {

	@FXML
	private TextField cardNumber;
	@FXML
	private TextField cardType;
	@FXML
	private DatePicker expDate;
	@FXML
	private TextField cvv;
	@FXML
	private TextField active;
	@FXML
	private TextField addressLineOne;
	@FXML
	private TextField addressLineTwo;
	@FXML
	private TextField city;
	@FXML
	private ComboBox statesComboBox;
	@FXML
	private TextField zip;
	@FXML
	private TextField country;
	@FXML
	private TextField firstName;
	@FXML
	private TextField lastName;
	@FXML
	private TextField txtFieldPaymentID;
	
	/**
	 * This method sends all of the address info to the
	 * model and then creates an address in the database.
	 * @param event
	 */
	@FXML
	private void createPayment(ActionEvent event) {
		PaymentDao pDao = new PaymentDao();
		Payment payment = new Payment();
		AddressDao aDao = new AddressDao();
		Address address = new Address();
		User user = Main.getInstance().getUser();
		
		payment.setCardNumber(cardNumber.getText());
		payment.setCardType(cardType.getText());
		Date date = Date.valueOf(expDate.getValue());
		payment.setExpDate(date);
		payment.setCvv(cvv.getText());
		payment.setActive(active.getText());
		payment.setUserID(user.getUserID());
		
		address.setAddressLastName(lastName.getText());
		address.setAddressFirstName(firstName.getText());
		address.setAddressLineOne(addressLineOne.getText());
		address.setAddressLineTwo(addressLineTwo.getText());
		address.setCity(city.getText());
		//Will need to fix this states option later
		address.setState("AR");
		address.setZip(zip.getText());
		address.setCountry(country.getText());
		address.setUserID(user.getUserID());
		
		//the two SQL queries that will be executed to create a payment
		aDao.create(address);
		pDao.create(payment);
		
		clearTextFields();
	}
	@FXML
	private void loadPayment(ActionEvent event) {
		boolean checkField = true;
		if (checkTextFieldPaymentID(checkField) == false) {
			Alert alert = new Alert(AlertType.WARNING);
    		alert.setTitle("Error");
    		alert.setContentText("Please enter an address ID to load an address.");
    		alert.showAndWait();
		} else if (checkTextFieldPaymentID(checkField) == true) {
		validatePayment();
		
		PaymentDao pDao = new PaymentDao();
		Main.getInstance().setPayment(pDao.get(Integer.parseInt(txtFieldPaymentID.getText())));
		Payment payment = Main.getInstance().getPayment();
		
		cardNumber.setText(payment.getCardNumber());
		cardType.setText(payment.getCardType());
		expDate.setValue(payment.getExpDate().toLocalDate());
		cvv.setText(payment.getCvv());
		cardType.setText(payment.getCardType());
		active.setText(payment.getActive());
		
		AddressDao aDao = new AddressDao();
		Main.getInstance().setAddress(aDao.get(payment.getAddressID()));
		Address address = Main.getInstance().getAddress();
		
		addressLineOne.setText(address.getAddressLineOne());
		addressLineTwo.setText(address.getAddressLineTwo());
		city.setText(address.getCity());
		zip.setText(address.getZip());
		country.setText(address.getCountry());
		firstName.setText(address.getAddressFirstName());
		lastName.setText(address.getAddressLastName());
		
		}
	}
	@FXML
	private void deletePayment(ActionEvent event) {
		boolean checkField = true;
		if (checkTextFieldPaymentID(checkField) == false) {
			Alert alert = new Alert(AlertType.WARNING);
    		alert.setTitle("Error");
    		alert.setContentText("Please enter a payment ID to delete a payment and related address.");
    		alert.showAndWait();
		} else if (checkTextFieldPaymentID(checkField) == true) {
		//to see if the address is even in the database
		validatePayment();
		PaymentDao pDao = new PaymentDao();
		Payment payment = new Payment();
		payment.setPaymentID(Integer.parseInt(txtFieldPaymentID.getText()));
		pDao.delete(payment);
		clearTextFields();
		}
	}
	private void clearTextFields() {
		cardNumber.setText("");
		cardType.setText("");
		expDate.setValue(null);
		cvv.setText("");
		active.setText("");
		firstName.setText("");
		lastName.setText("");
		addressLineOne.setText("");
		addressLineTwo.setText("");
		city.setText("");
		//states.set(2, String);
		zip.setText("");
		country.setText("");
	}
	/**
	 * Helps checking to see if the addressID text field has
	 * any input. The method that calls this method will throw
	 * a javaFX alert.
	 * @param pFlag
	 * @return
	 */
	private boolean checkTextFieldPaymentID(boolean pFlag) {
		boolean flag;
		if (txtFieldPaymentID.getText().equals("")) {
			flag = false;
		} else {
			flag = true;
		}
		return flag;	
	}
	/**
	 * Checks to see if the address exists in the database
	 */
	private void validatePayment() {
		PaymentDao pDao = new PaymentDao();
		Main.getInstance().setPayment(pDao.get(Integer.parseInt(txtFieldPaymentID.getText())));
		Payment payment = Main.getInstance().getPayment();
		
		int paymentID = payment.getPaymentID();
		if (paymentID == 0) {
			Alert alert = new Alert(AlertType.WARNING);
	    	alert.setTitle("Error");
	    	alert.setContentText("There are no payment methods in the database that match this ID.");
	    	alert.showAndWait();
		}
	}
	
}
