package controller;

import java.io.IOException;
import java.util.ArrayList;
import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import model.Address;
import model.AddressDao;
import model.User;

public class AddressController {

	// ------------------Variables linked to the EditAddress.fxml----------------
	@FXML
	private TextField addressFirstName;
	@FXML
	private TextField addressLastName;
	@FXML
	private TextField addressLineOne;
	@FXML
	private TextField addressLineTwo;
	@FXML
	private TextField city;
	@FXML
	private TextField zip;
	@FXML
	private TextField country;
	@FXML
	private TextField txtFieldAddressID;
	@FXML
	private Button btnUpdateAddress;

	// -----------Variables for the AddressCardHolder.fxml-----
	@FXML
	private FlowPane addressCardHolder;
	@FXML
	private AnchorPane addressAnchor;

	// ---------------Variables for AddressCard.fxml------------
	@FXML
	private Label firstNameAddressCard;
	@FXML
	private Label lastNameAddressCard;
	@FXML
	private Label addressLineOneCard;
	@FXML
	private Label addressLineTwoCard;
	@FXML
	private Label cityStateZip;
	@FXML
	private Button btnEdit;

	@FXML
	public void initialize() throws IOException {
		ArrayList<Address> userAddresses = new ArrayList<Address>();
		AddressDao aDao = new AddressDao();
		int userid = Main.getInstance().getUser().getUserID();
		userAddresses = (ArrayList<Address>) aDao.getAllbyID(userid);
		Main.getInstance().getAddress().setUserAddresses(userAddresses);

		for (int i = 0; i < userAddresses.size(); i++) {
			AnchorPane ap = new AnchorPane();
			ap.setStyle("-fx-border-style: solid;");

			firstNameAddressCard = new Label();
			lastNameAddressCard = new Label();
			addressLineOneCard = new Label();
			addressLineTwoCard = new Label();
			cityStateZip = new Label();
			btnEdit = new Button();

			addressCardHolder.getChildren().add(ap);

			ap.getChildren().add(firstNameAddressCard);
			firstNameAddressCard.setLayoutY(20);
			ap.getChildren().add(lastNameAddressCard);
			lastNameAddressCard.setLayoutY(40);
			ap.getChildren().add(addressLineOneCard);
			addressLineOneCard.setLayoutY(60);
			ap.getChildren().add(addressLineTwoCard);
			ap.getChildren().add(cityStateZip);
			cityStateZip.setLayoutY(80);
			ap.getChildren().add(btnEdit);
			btnEdit.setLayoutY(100);
			// btnEdit.addEventHandler(ActionEvent, eventHandler);

			firstNameAddressCard.setText(userAddresses.get(i).getAddressFirstName());
			firstNameAddressCard.setText(userAddresses.get(i).getAddressFirstName());
			lastNameAddressCard.setText(userAddresses.get(i).getAddressLastName());
			addressLineOneCard.setText(userAddresses.get(i).getAddressLineOne());

			if (userAddresses.get(i).getAddressLineTwo().equals("")) {
				addressLineTwoCard.setVisible(false);
			} else if (!userAddresses.get(i).getAddressLineTwo().equals("")) {
				addressLineTwoCard.setText(userAddresses.get(i).getAddressLineTwo());
				addressLineTwoCard.setLayoutY(80);
				cityStateZip.setLayoutY(100);
				btnEdit.setLayoutY(120);
			}

			cityStateZip.setText(userAddresses.get(i).getCity() + ", " + userAddresses.get(i).getState() + " "
					+ userAddresses.get(i).getZip());

			System.out.println("Address " + i);
		}
	}

	// -----------------------Actions fired from the EditAddress.fxml---------------
	
	public void loadEditAddress(ActionEvent event) throws Exception {
		MainController mc = new MainController();
		mc.subWindow(event);
	}
	
	/**
	 * I've created an enumeration for all of these state values but I'm not sure
	 * how to put it into the comboBox, so I will leave them stored in this list
	 * instead for now.
	 */
	ObservableList<String> states = FXCollections.observableArrayList("Alaska", "Arizona", "Arkansas", "California",
			"Colorado", "CT", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
			"Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota",
			"Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico",
			"New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island",
			"South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington",
			"West Virginia", "Wisconsin", "Wyoming");

	// @FXML
	// private ComboBox<String> statesComboBox;
	// /**
	// * Initializes the comboBox with
	// * all of the list options defined above.
	// */
	// @FXML
	// private void initialize() {
	// statesComboBox.setValue("Alabama");
	// statesComboBox.setItems(states);
	// }
	/**
	 * This method sends all of the address info to the model and then creates an
	 * address in the database.
	 * 
	 * @param event
	 */
	@FXML
	private void createAddress(ActionEvent event) {
		Address address = new Address();
		AddressDao aDao = new AddressDao();
		User user = Main.getInstance().getUser();

		address.setAddressLineOne(addressLineOne.getText());
		address.setAddressLineTwo(addressLineTwo.getText());
		address.setCity(city.getText());

		// will need to figure out how to code how to get the index that is
		// selected in the combo box
		address.setState(states.get(5));
		address.setZip(zip.getText());
		address.setCountry(country.getText());
		address.setAddressLastName(addressLastName.getText());
		address.setAddressFirstName(addressFirstName.getText());
		address.setUserID(user.getUserID());
		aDao.create(address);
		clearTextFields();
	}

	@FXML
	private void loadAddress(ActionEvent event) {
		boolean checkField = true;
		if (checkTextFieldAddressID(checkField) == false) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setContentText("Please enter an address ID to load an address.");
			alert.showAndWait();
		} else if (checkTextFieldAddressID(checkField) == true) {
			validateAddress();

			AddressDao aDao = new AddressDao();
			Main.getInstance().setAddress(aDao.get(Integer.parseInt(txtFieldAddressID.getText())));
			Address address = Main.getInstance().getAddress();

			addressFirstName.setText(address.getAddressFirstName());
			addressLastName.setText(address.getAddressLastName());
			addressLineOne.setText(address.getAddressLineOne());
			addressLineTwo.setText(address.getAddressLineTwo());
			city.setText(address.getCity());
			// states.set(2, String);
			zip.setText(address.getZip());
			country.setText(address.getCountry());

			btnUpdateAddress.setDisable(false);
		}
	}

	@FXML
	private void deleteAddress(ActionEvent event) {
		boolean checkField = true;
		if (checkTextFieldAddressID(checkField) == false) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setContentText("Please enter an address ID to delete an address.");
			alert.showAndWait();
		} else if (checkTextFieldAddressID(checkField) == true) {
			// to see if the address is even in the database
			validateAddress();
			AddressDao aDao = new AddressDao();
			Address address = new Address();
			address.setAddressID(Integer.parseInt(txtFieldAddressID.getText()));
			aDao.delete(address);
			clearTextFields();
		}
	}

	@FXML
	private void updateAddress(ActionEvent event) {
		boolean checkField = true;
		if (checkTextFieldAddressID(checkField) == false) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setContentText("Please enter an address ID to update an address.");
			alert.showAndWait();
		} else if (checkTextFieldAddressID(checkField) == true) {
			Address address = Main.getInstance().getAddress();
			address.setAddressLineOne(addressLineOne.getText());
			address.setAddressLineTwo(addressLineTwo.getText());
			address.setCity(city.getText());

			// will need to figure out how to code how to get the index that is
			// selected in the combo box
			address.setState(states.get(5));
			address.setZip(zip.getText());
			address.setCountry(country.getText());
			address.setAddressLastName(addressLastName.getText());
			address.setAddressFirstName(addressFirstName.getText());
			AddressDao aDao = new AddressDao();
			aDao.update(address);
		}
	}
	/**
	 * Checks to see if the address exists in the database
	 */
	private void validateAddress() {
		AddressDao aDao = new AddressDao();
		Main.getInstance().setAddress(aDao.get(Integer.parseInt(txtFieldAddressID.getText())));
		Address address = Main.getInstance().getAddress();

		int addressID = address.getAddressID();
		if (addressID == 0) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setContentText("There are no addresses in the database that match this ID.");
			alert.showAndWait();
		}
	}
	/**
	 * Helps checking to see if the addressID text field has any input. The method
	 * that calls this method will throw a javaFX alert.
	 * 
	 * @param pFlag
	 * @return
	 */
	private boolean checkTextFieldAddressID(boolean pFlag) {
		boolean flag;
		if (txtFieldAddressID.getText().equals("")) {
			flag = false;
		} else {
			flag = true;
		}
		return flag;
	}

	private void clearTextFields() {
		addressFirstName.setText("");
		addressLastName.setText("");
		addressLineOne.setText("");
		addressLineTwo.setText("");
		city.setText("");
		// states.set(2, String);
		zip.setText("");
		country.setText("");
	}
}
