package controller;

import java.io.IOException;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.User;
import model.UserDao;

public class UserController {

	private MasterController master;

	// --------Communication with the Master Controller--------------
	public void init(MasterController masterController) {
		master = masterController;
	}

	// ------------------Variables linked to the SignIn.fxml--------------------
	@FXML
	private TextField emailSignIn;
	@FXML
	private PasswordField passwordSignIn;
	@FXML
	private AnchorPane rootPane;

	// ------------------Variables linked to the NewUser.fxml--------------------
	@FXML
	private TextField firstNameNewUser;
	@FXML
	private TextField lastNameNewUser;
	@FXML
	private TextField emailNewUser;
	@FXML
	private PasswordField passwordNewUserOne;
	@FXML
	private PasswordField passwordNewUserTwo;

	// ----------------------Actions fired from the SignIn.fxml----------------
	/**
	 * This method loads the "NewUser" creation page from the sign in page by
	 * setting the contents in the sing in page to the contents of the "NewUser"
	 * page.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void loadNewUser(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("../view/NewUser.fxml"));
		rootPane.getChildren().setAll(pane);
	}

	/**
	 * This is the signIn action event.
	 * 
	 * @param event
	 * @throws IOException 
	 */
	@FXML
	private void signInActionEvent(ActionEvent event) throws IOException {
		UserDao ud = new UserDao();
		Main.getInstance().setUser(ud.get(emailSignIn.getText(), passwordSignIn.getText()));
		User user = Main.getInstance().getUser();

		if (user.getUserID() == 0) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setContentText("The username or password does not match any accounts in the database.");
			alert.showAndWait();
		} else if (user != null) {
			// this code simply closes the log in screen when it is done
			Node source = (Node) event.getSource();
			Stage stage = (Stage) source.getScene().getWindow();
			stage.close();
		}
		System.out.println(user.getUserID());
		
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/MainMenu.fxml"));
		Parent root = (Parent) fxmlLoader.load();
		Stage stage = new Stage();
		stage.setScene(new Scene(root));
		stage.show();
	}

	// -----------------------------Actions fired from NewUser.fxml------------
	/**
	 * This event brings the user to a new window to create an account.
	 * 
	 * @param event
	 * @throws Exception
	 */
	@FXML
	private void createNewUser(ActionEvent event) throws Exception {
		if (firstNameNewUser.getText().equals(""))
			System.out.println("Finish filling out fields");
		else if (lastNameNewUser.getText().equals(""))
			System.out.println("Finish filling out fields");
		else if (emailNewUser.getText().equals(""))
			System.out.println("Finish filling out fields");
		else if (passwordNewUserOne.getText().equals(""))
			System.out.println("Finish filling out fields");
		else if (passwordNewUserTwo.getText().equals(""))
			System.out.println("Finish filling out fields");
		else {
			boolean match = true;
			confirmPasswordMatch(match);
			User user = Main.getInstance().getUser();
			UserDao uDao = new UserDao();
			if (confirmPasswordMatch(match) == true) {
				user.setFirstName(firstNameNewUser.getText());
				user.setLastName(lastNameNewUser.getText());
				user.setEmail(emailNewUser.getText());
				user.setPassword(passwordNewUserOne.getText());
				uDao.create(user);
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Account Creation Confirmed");
				alert.setContentText("Your account has been succesfully created.");
				alert.showAndWait();
				// the user will be redirected to the SignIn.fxml
				loadSignIn(event);
			} else {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Error");
				alert.setContentText("The password fields do not match. Reenter the passwords.");
				alert.showAndWait();
				System.out.println("No Info Saved to database.");
			}
		}
	}

	/**
	 * This method confirms if the password matches and passes and parameter as a
	 * boolean to the code that calls it.
	 * 
	 * @param pMatch
	 * @return
	 */
	private boolean confirmPasswordMatch(boolean pMatch) {
		if (passwordNewUserOne.getText().equals(passwordNewUserTwo.getText())) {
			pMatch = true;
			System.out.println("Match!");
		} else {
			System.out.println("Password: " + passwordNewUserOne.getText());
			System.out.println("Confirm password: " + passwordNewUserTwo.getText());
			System.out.println("Not a match! Info failed to save.");
			pMatch = false;
		}
		return pMatch;
	}

	/**
	 * This method takes the user from the "NewUser" creation window back to the
	 * sign in page.
	 * 
	 * @param event
	 * @throws Exception
	 */
	@FXML
	public void loadSignIn(ActionEvent event) throws Exception {
		Parent tableViewParent = FXMLLoader.load(getClass().getResource("../view/SignIn.fxml"));
		Scene tableViewScene = new Scene(tableViewParent);
		// this line gets the stage information
		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window.setScene(tableViewScene);
		window.show();
	}

	/**
	 * Closes the new user creation page with the cancel button
	 * 
	 * @param event
	 * @throws Exception 
	 */
	@FXML
	public void cancelNewUser(ActionEvent event) throws Exception {
		loadSignIn(event);
	}

}
