package controller;

import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDate;

import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Address;
import model.AddressDao;
import model.Book;
import model.BookDao;
import model.User;
import model.UserDao;

public class MainController {

	// ------------------Variables linked to the SignIn.fxml--------------------
	@FXML
	private TextField emailSignIn;
	@FXML
	private PasswordField passwordSignIn;

	// ------------------Variables linked to the MainMenu.fxml--------------------
	/**
	 * Grabs the button pressed from the main FXML file
	 */
	@FXML
	private Button btn;
	/**
	 * Variable that works with loading the secondary page inside the main page
	 */
	@FXML
	private AnchorPane centerRootPane;
	/**
	 * Variable that works with loading the secondary page inside the right side of
	 * the main page
	 */
	@FXML
	private AnchorPane rightRootPane;
	@FXML
	private Label title;
	@FXML
	private Button btnSignInMain;
	@FXML
	private Button btnEditAddress;
	@FXML
	private Button btnEditPayment;
	@FXML
	private Button btnEditPhone;
	@FXML
	private FlowPane leftFlowPane;
	@FXML
	private Label welcomeLabel;

	// ---------------Variables linked to the book pages--------------
	// -------------Everything in the book is created from initialize
	@FXML
	private TextField txtISBN;
	@FXML
	private TextField txtPublicationID;
	@FXML
	private TextField txtBookName;
	@FXML
	private TextField txtAuthor;
	@FXML
	private TextField txtRetailPrice;
	@FXML
	private TextField txtBinding;
	@FXML
	private TextField txtQuantityOnHand;
	@FXML
	private TextField txtWholesale;
	@FXML
	private TextField txtPublicationDate;
	@FXML
	private ImageView bookImage;
	@FXML
	private GridPane bookGrid;
	@FXML
	private TextField txtSearch;
	@FXML
	private Label lblBookName;
	@FXML
	private GridPane genreGrid;
	@FXML
	private Button bookButton;
	@FXML
	private AnchorPane AdminBook;
	@FXML
	private Tab AdminTabBook;
	@FXML
	private TabPane tabPane;
	@FXML
	private Label lblAddressLine1;
	@FXML
	private Label lblState;
	@FXML
	private Label lblPublisherName;
	@FXML
	private Label lblPublisherContactName;
	@FXML
	private Label lblPublisherNumber;
	@FXML
	private Label lblQuantity;
	@FXML
	private Label lblISBN;
	@FXML
	private Label lblBinding;
	@FXML
	private Label lblCopyrightDate;
	@FXML
	private Label lblRetailPrice;
	@FXML
	private Label lblAuthor;
	@FXML
	private Label lblZip;
	@FXML
	private Label lblCity;
	@FXML
	private Label lblCountry;
	@FXML
	private GridPane BookInfoGrid;

	Book myBook = new Book();
	BookDao daoBook = new BookDao();

	// ------------------Variables linked to the EditAddress.fxml----------------
	@FXML
	private TextField addressFirstName;
	@FXML
	private TextField addressLastName;
	@FXML
	private TextField addressLineOne;
	@FXML
	private TextField addressLineTwo;
	@FXML
	private TextField city;
	@FXML
	private TextField zip;
	@FXML
	private TextField country;
	@FXML
	private TextField txtFieldAddressID;
	@FXML
	private Button btnUpdateAddress;

	// -----------Variables for the AddressCardHolder.fxml-----
	@FXML
	private FlowPane addressCardHolder;
	@FXML
	private AnchorPane addressAnchor;

	// ---------------Variables for AddressCard.fxml------------
	@FXML
	private Label firstNameAddressCard;
	@FXML
	private Label lastNameAddressCard;
	@FXML
	private Label addressLineOneCard;
	@FXML
	private Label addressLineTwoCard;
	@FXML
	private Label cityStateZip;
	@FXML
	private Button btnEditAdd;
	@FXML
	private GridPane addressGrid;
	
	Address address = new Address();
	AddressDao addressDao = new AddressDao();

	// --------------------Method that initializes the book pages----
	@FXML
	public void initialize() {
		String name = Main.getInstance().getUser().getFirstName();
		welcomeLabel.setText("Walcome " + name);
		// this is to generate the genre buttons

		// this is to load up all the books on the home page
		int innerRow = 0;
		int innerCol = 0;
		int row = 1;
		int col = 0;
		for (Book e : daoBook.getAllBooks()) {
			if ((col % 5) == 0 && col != 0) {
				row = row + 3;
				col = 0;
			}
			GridPane innerGrid = new GridPane();
			ImageView bookImage = new ImageView();
			bookButton = new Button("View");
			Image img = new Image("images/" + e.getBookID() + ".jpg");
			Label bookName = new Label();
			Label bookAuthor = new Label();
			Label bookRetailPrice = new Label();
			bookGrid.add(innerGrid, col, row);
			innerGrid.setStyle("-fx-grid-lines-visible: true");

			bookImage.setFitHeight(100);
			bookImage.setFitWidth(100);
			bookImage.setImage(img);
			innerGrid.add(bookImage, innerCol, innerRow);

			bookName.setText(" " + e.getBookName());
			innerGrid.add(bookName, innerCol, innerRow + 2);
			bookAuthor.setText(" By " + e.getAuthor());
			innerGrid.add(bookAuthor, innerCol, innerRow + 3);

			DecimalFormat df2 = new DecimalFormat(".00");
			bookRetailPrice.setText(" $" + df2.format(e.getRetailPrice()));

			innerGrid.add(bookRetailPrice, innerCol, innerRow + 4);
			innerGrid.add(bookButton, innerCol, innerRow + 5);
			bookButton.setId(String.valueOf(e.getBookID()));
			int n = e.getBookID();
			bookButton.setOnAction((event) -> {
				GetBook(n);
			});

			innerCol = innerCol + 1;
			col = col + 1;
		}
		tabPane.getSelectionModel().select(1);

		// this is to load up all the addresses by userID on the home page
		int addressInnerRow = 0;
		int addressInnerCol = 0;
		int addressRow = 1;
		int addressCol = 0;
		for (Address a : addressDao.getAllbyID(Main.getInstance().getUser().getUserID())) {
			if ((addressCol % 5) == 0 && addressCol != 0) {
				addressRow = addressRow + 3;
				addressCol = 0;
			}
			GridPane innerGrid = new GridPane();
			
			btnEditAdd = new Button("Edit");
			
			Label addressName = new Label();
			Label addressFirstLine = new Label();
			Label addressSecondLine = new Label();
			Label addressCityStateZip = new Label();
			Label addressCountry = new Label();
			addressGrid.add(innerGrid, addressCol, addressRow);
			innerGrid.setStyle("-fx-grid-lines-visible: true");

			addressName.setText(" " + a.getAddressFirstName() + " " + a.getAddressLastName());
			innerGrid.add(addressName, addressInnerCol, addressInnerRow + 2);
			addressFirstLine.setText(" " + a.getAddressLineOne());
			innerGrid.add(addressFirstLine, addressInnerCol, addressInnerRow + 3);
			
			int n = 0;	
			if (!a.getAddressLineTwo().equals("")) {
			addressSecondLine.setText(" " + a.getAddressLineTwo());
			n = 1;
			innerGrid.add(addressSecondLine, addressInnerCol, addressInnerRow + 4);
			}
			else if (a.getAddressLineTwo().equals("")) {
			n = 0;
			}
			
			addressCityStateZip.setText(" " + a.getCity() + ", " + a.getState() + " " + a.getZip());
			innerGrid.add(addressCityStateZip, addressInnerCol, addressInnerRow + 4 + n);
			addressCountry.setText(" " + a.getCountry());
			innerGrid.add(addressCountry, addressInnerCol, addressInnerRow + 5 + n);
			
			innerGrid.add(btnEditAdd, addressInnerCol, addressInnerRow + 6 + n);
			btnEditAdd.setId(String.valueOf(a.getAddressID()));
			btnEditAdd.setOnAction((event) -> {
				//GetAddress(a.getAddressID());
			});

			addressInnerCol = addressInnerCol + 1;
			addressCol = addressCol + 1;
		}

	}

	@FXML
	void Search() {
		tabPane.getSelectionModel().select(1);
		int innerRow = 0;
		int innerCol = 0;
		int row = 1;
		int col = 0;
		bookGrid.getChildren().clear();

		for (Book e : daoBook.searchAll(txtSearch.getText())) {
			if ((col % 5) == 0 && col != 0) {
				row = row + 3;
				col = 0;
			}
			GridPane innerGrid = new GridPane();
			ImageView bookImage = new ImageView();
			bookButton = new Button("View");
			Image img = new Image("images/" + e.getBookID() + ".jpg");
			Label bookName = new Label();
			Label bookAuthor = new Label();
			Label bookRetailPrice = new Label();
			bookGrid.add(innerGrid, col, row);
			innerGrid.setStyle("-fx-grid-lines-visible: true");

			bookImage.setFitHeight(100);
			bookImage.setFitWidth(100);
			bookImage.setImage(img);
			innerGrid.add(bookImage, innerCol, innerRow);

			bookName.setText(" " + e.getBookName());
			innerGrid.add(bookName, innerCol, innerRow + 2);
			bookAuthor.setText(" By " + e.getAuthor());
			innerGrid.add(bookAuthor, innerCol, innerRow + 3);
			DecimalFormat df2 = new DecimalFormat(".00");
			bookRetailPrice.setText(" $" + df2.format(e.getRetailPrice()));
			innerGrid.add(bookRetailPrice, innerCol, innerRow + 4);
			innerGrid.add(bookButton, innerCol, innerRow + 5);
			bookButton.setId(String.valueOf(e.getBookID()));
			int n = e.getBookID();
			bookButton.setOnAction((event) -> {
				GetBook(n);
			});

			innerCol = innerCol + 1;
			col = col + 1;
		}
	}
	
	@FXML
	void addressTab() {
		tabPane.getSelectionModel().select(3);
	}

	@FXML
	void CreateBook() {
		myBook.setISBN((txtISBN.getText()));
		myBook.setPublicationID(Integer.parseInt(txtPublicationID.getText()));
		myBook.setBookName(txtBookName.getText());
		myBook.setAuthor(txtAuthor.getText());
		myBook.setRetailPrice(Double.parseDouble(txtRetailPrice.getText()));
		myBook.setBinding((txtBinding.getText()));
		myBook.setQuantityOnHand(Integer.parseInt(txtQuantityOnHand.getText()));
		myBook.setWholesale((Double.parseDouble(txtWholesale.getText())));
		myBook.setPublicationDate((LocalDate.parse(txtPublicationDate.getText())));
		daoBook.create(myBook);
	}

	void GetBook(int pBookID) {
		myBook = daoBook.get(pBookID);
		// This creates the publication class with the correct book
		myBook.myPublication = myBook.daoPublication.get(myBook.getPublicationID());
		// this changes the index of the tab pane to the BookInfo
		tabPane.getSelectionModel().select(2);

		lblISBN.setText(String.valueOf(myBook.getISBN()));
		lblBookName.setText(myBook.getBookName());
		lblAuthor.setText(myBook.getAuthor());
		DecimalFormat df2 = new DecimalFormat(".00");
		lblRetailPrice.setText(df2.format(myBook.getRetailPrice()));
		lblBinding.setText(myBook.getBinding());
		lblQuantity.setText(String.valueOf(myBook.getQuantityOnHand()));

		Image img = new Image("images/" + myBook.getBookID() + ".jpg");
		ImageView bookImage = new ImageView();
		bookImage.setFitHeight(100);
		bookImage.setFitWidth(100);
		bookImage.setImage(img);
		BookInfoGrid.add(bookImage, 0, 8);
		lblPublisherName.setText(myBook.myPublication.getPublisherName());
		lblPublisherContactName.setText(myBook.myPublication.getContactName());
		lblCopyrightDate.setText(String.valueOf(myBook.myPublication.getCopyrightDate()));

		// TODO: waiting on the address class and the phone class to finish this

		/*
		 * lblPublisherNumber.setText(myBook.myPublication.get
		 * lblAddressLine1.setText(myBook.myPublication.get lblCity lblState lblZip
		 * lblCournty
		 */

		txtISBN.setText(String.valueOf(myBook.getISBN()));
		txtPublicationID.setText(String.valueOf(myBook.getPublicationID()));
		txtBookName.setText(myBook.getBookName());
		txtAuthor.setText(myBook.getAuthor());

		txtRetailPrice.setText(df2.format(myBook.getRetailPrice()));
		txtBinding.setText(myBook.getBinding());
		txtQuantityOnHand.setText(String.valueOf(myBook.getQuantityOnHand()));
		txtWholesale.setText(String.valueOf(myBook.getWholesale()));
		txtPublicationDate.setText(String.valueOf(myBook.getPublicationDate()));

		// Image img = new Image("images/" + myBook.getBookID() + ".jpg");
		// //bookImage.setImage(img);
		// bookImage.setFitHeight(100);
		// bookImage.setFitWidth(100);
		// return null;

	}

	@FXML
	void BackToBooks() {
		tabPane.getSelectionModel().select(1);
	}
	/*
	 * @FXML void runUpdateBook() {
	 * myBook.setISBN(Integer.parseInt(txtISBN.getText()));
	 * myBook.setBookName(txtBookName.getText());
	 * myBook.setAuthor(txtAuthor.getText());
	 * myBook.setPrice(Double.parseDouble(txtPrice.getText()));
	 * daoBook.update(myBook); }
	 * 
	 * @FXML void runDeleteBook() {
	 * myBook.setISBN(Integer.parseInt(txtISBN.getText())); daoBook.delete(myBook);
	 * runClear(); }
	 * 
	 * @FXML void runClear() { txtISBN.clear(); txtBookName.clear();
	 * txtAuthor.clear(); txtPrice.clear(); }
	 * 
	 * @FXML void runKeyPressed(KeyEvent e) { if (e.getCode() == KeyCode.DELETE) {
	 * e.consume(); //runClearOutput(); } }
	 */

	// -------------------Actions fired from the MainMenu.fxml-------------------
	/**
	 * This method is for creating an additional window for an FXML "stage"
	 * 
	 * @param event
	 * @throws Exception
	 */
	public void newWindow(ActionEvent event) throws Exception {
		// this code creates a button object, which contains a method that can grab
		// the id of the button pressed
		btn = (Button) event.getSource();
		String id = btn.getId();
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(determinePath(id)));
		Parent root = (Parent) fxmlLoader.load();
		Stage stage = new Stage();
		// this modal disables the main stage before this stage is open
		stage.initModality(Modality.APPLICATION_MODAL);
		// this code is to set the scene
		stage.setScene(new Scene(root));
		stage.show();
	}

	/**
	 * This method is for creating a "sub" stage with in the main "stage"
	 * 
	 * @param event
	 * @throws Exception
	 */
	public void subWindow(ActionEvent event) throws Exception {
		String id;
		btn = (Button) event.getSource();
		id = btn.getId();
		AnchorPane centerPane = FXMLLoader.load(getClass().getResource(determinePath(id)));
		centerRootPane.getChildren().setAll(centerPane);

		if (id.equals("btnEditAddress")) {
			id = "createAddress";
			title.setText("Shipping Addresses");
		} else if (id.equals("btnEditPayment")) {
			id = "createPayment";
			title.setText("Payment Methods");
		} else if (id.equals("btnEditPhone")) {
			id = "createPhone";
			title.setText("Phone Numbers");
		}

		AnchorPane rightPane = FXMLLoader.load(getClass().getResource(determinePath(id)));
		rightRootPane.getChildren().setAll(rightPane);
	}

	/**
	 * This method takes a parameter of the string id from the FXML button
	 * attributes and then changes it into a the proper build path
	 * 
	 * @param pid
	 * @return
	 */
	public String determinePath(String pid) {
		// this conditional sets the correct path for the FXML file
		// depending on the button pressed
		String fxmlPath = null;

		if (pid.equals("btnSignInMain"))
			fxmlPath = "../view/SignIn.fxml";
		else if (pid.equals("btnNewUser"))
			fxmlPath = "../view/NewUser.fxml";

		else if (pid.equals("btnBestSellers"))
			fxmlPath = "../view/BestSellers.fxml";

		else if (pid.equals("btnEditAddress"))
			fxmlPath = "../view/AddressCardHolder.fxml";
		else if (pid.equals("createAddress"))
			fxmlPath = "../view/EditAddress.fxml";

		else if (pid.equals("btnEditPayment"))
			fxmlPath = "../view/PaymentCardHolder.fxml";
		else if (pid.equals("createPayment"))
			fxmlPath = "../view/EditPayment.fxml";

		else if (pid.equals("btnEditPhone"))
			fxmlPath = "../view/PhoneCardHolder.fxml";
		else if (pid.equals("createPhone"))
			fxmlPath = "../view/EditPhone.fxml";

		return fxmlPath;
	}

	// -------------------------Actions fired from the
	// SignIn.fxml-------------------
	/**
	 * This method loads the "NewUser" creation page from the sign in page by
	 * setting the contents in the sing in page to the contents of the "NewUser"
	 * page.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void loadNewUser(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("../view/NewUser.fxml"));
		centerRootPane.getChildren().setAll(pane);
	}

	/**
	 * This is the signIn action event.
	 * 
	 * @param event
	 */
	@FXML
	private void signInActionEvent(ActionEvent event) {
		UserDao ud = new UserDao();
		Main.getInstance().setUser(ud.get(emailSignIn.getText(), passwordSignIn.getText()));
		User user = Main.getInstance().getUser();

		if (user.getUserID() == 0) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setContentText("The username or password does not match any accounts in the database.");
			alert.showAndWait();
		} else if (user != null) {
			// this code simply closes the log in screen when it is done
			Node source = (Node) event.getSource();
			Stage stage = (Stage) source.getScene().getWindow();
			stage.close();
		}
		System.out.println(user.getUserID());
	}

	/**
	 * This event brings the user to a new window to create an account.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void newUserActionEvent(ActionEvent event) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("NewUser.fxml"));
		centerRootPane.getChildren().setAll(pane);
	}

	// -----------------------Actions fired from the EditAddress.fxml---------------
	public void loadEditAddress(ActionEvent event) throws Exception {
		MainController mc = new MainController();
		mc.subWindow(event);
	}

	/**
	 * I've created an enumeration for all of these state values but I'm not sure
	 * how to put it into the comboBox, so I will leave them stored in this list
	 * instead for now.
	 */
	ObservableList<String> states = FXCollections.observableArrayList("Alaska", "Arizona", "Arkansas", "California",
			"Colorado", "CT", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
			"Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota",
			"Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico",
			"New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island",
			"South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington",
			"West Virginia", "Wisconsin", "Wyoming");

	// @FXML
	// private ComboBox<String> statesComboBox;
	// /**
	// * Initializes the comboBox with
	// * all of the list options defined above.
	// */
	// @FXML
	// private void initialize() {
	// statesComboBox.setValue("Alabama");
	// statesComboBox.setItems(states);
	// }
	/**
	 * This method sends all of the address info to the model and then creates an
	 * address in the database.
	 * 
	 * @param event
	 */
	@FXML
	private void createAddress(ActionEvent event) {
		Address address = new Address();
		AddressDao aDao = new AddressDao();
		User user = Main.getInstance().getUser();

		address.setAddressLineOne(addressLineOne.getText());
		address.setAddressLineTwo(addressLineTwo.getText());
		address.setCity(city.getText());

		// will need to figure out how to code how to get the index that is
		// selected in the combo box
		address.setState(states.get(5));
		address.setZip(zip.getText());
		address.setCountry(country.getText());
		address.setAddressLastName(addressLastName.getText());
		address.setAddressFirstName(addressFirstName.getText());
		address.setUserID(user.getUserID());
		aDao.create(address);
		clearTextFields();
	}

	@FXML
	private void loadAddress(ActionEvent event) {
		boolean checkField = true;
		if (checkTextFieldAddressID(checkField) == false) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setContentText("Please enter an address ID to load an address.");
			alert.showAndWait();
		} else if (checkTextFieldAddressID(checkField) == true) {
			validateAddress();

			AddressDao aDao = new AddressDao();
			Main.getInstance().setAddress(aDao.get(Integer.parseInt(txtFieldAddressID.getText())));
			Address address = Main.getInstance().getAddress();

			addressFirstName.setText(address.getAddressFirstName());
			addressLastName.setText(address.getAddressLastName());
			addressLineOne.setText(address.getAddressLineOne());
			addressLineTwo.setText(address.getAddressLineTwo());
			city.setText(address.getCity());
			// states.set(2, String);
			zip.setText(address.getZip());
			country.setText(address.getCountry());

			btnUpdateAddress.setDisable(false);
		}
	}

	@FXML
	private void deleteAddress(ActionEvent event) {
		boolean checkField = true;
		if (checkTextFieldAddressID(checkField) == false) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setContentText("Please enter an address ID to delete an address.");
			alert.showAndWait();
		} else if (checkTextFieldAddressID(checkField) == true) {
			// to see if the address is even in the database
			validateAddress();
			AddressDao aDao = new AddressDao();
			Address address = new Address();
			address.setAddressID(Integer.parseInt(txtFieldAddressID.getText()));
			aDao.delete(address);
			clearTextFields();
		}
	}

	@FXML
	private void updateAddress(ActionEvent event) {
		boolean checkField = true;
		if (checkTextFieldAddressID(checkField) == false) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setContentText("Please enter an address ID to update an address.");
			alert.showAndWait();
		} else if (checkTextFieldAddressID(checkField) == true) {
			Address address = Main.getInstance().getAddress();
			address.setAddressLineOne(addressLineOne.getText());
			address.setAddressLineTwo(addressLineTwo.getText());
			address.setCity(city.getText());

			// will need to figure out how to code how to get the index that is
			// selected in the combo box
			address.setState(states.get(5));
			address.setZip(zip.getText());
			address.setCountry(country.getText());
			address.setAddressLastName(addressLastName.getText());
			address.setAddressFirstName(addressFirstName.getText());
			AddressDao aDao = new AddressDao();
			aDao.update(address);
		}
	}

	/**
	 * Checks to see if the address exists in the database
	 */
	private void validateAddress() {
		AddressDao aDao = new AddressDao();
		Main.getInstance().setAddress(aDao.get(Integer.parseInt(txtFieldAddressID.getText())));
		Address address = Main.getInstance().getAddress();

		int addressID = address.getAddressID();
		if (addressID == 0) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setContentText("There are no addresses in the database that match this ID.");
			alert.showAndWait();
		}
	}

	/**
	 * Helps checking to see if the addressID text field has any input. The method
	 * that calls this method will throw a javaFX alert.
	 * 
	 * @param pFlag
	 * @return
	 */
	private boolean checkTextFieldAddressID(boolean pFlag) {
		boolean flag;
		if (txtFieldAddressID.getText().equals("")) {
			flag = false;
		} else {
			flag = true;
		}
		return flag;
	}

	private void clearTextFields() {
		addressFirstName.setText("");
		addressLastName.setText("");
		addressLineOne.setText("");
		addressLineTwo.setText("");
		city.setText("");
		// states.set(2, String);
		zip.setText("");
		country.setText("");
	}
}
