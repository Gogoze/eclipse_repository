package application;
	
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Address;
import model.Payment;
import model.User;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Main extends Application {
	/**
	 * Declares a User in the main loader for the Javafx window
	 * to be used in the model
	 */
	private User user = new User();
	private Address address = new Address();
	private Payment payment = new Payment();
	/**
	 * This declares the "instance" of the window
	 */
	private static Main instance;
	/**
	 * Sets the instance as the main
	 */
	public Main() {
		instance = this;
	}
	/**
	 * This makes the instance retrievable from the controller
	 * @return
	 */
	public static Main getInstance() {
		return instance;
	}
	
	@Override
	public void start(Stage primaryStage) {
		try {
			//this block of code loads the home page on run
			Parent root = FXMLLoader.load(getClass().getResource("../view/SignIn.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("../style/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
}
